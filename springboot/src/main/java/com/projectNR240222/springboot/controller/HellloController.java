package com.projectNR240222.springboot.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HellloController {

    @GetMapping("/")
    public String helloMessage(){
        return "Hello Controller";

    }
}
